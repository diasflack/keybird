(function(global) {
    "use strict";
    
    var KB = global.KB || {};
    
    KB.gameEngine = {
        
        keyBirdState: {
            preload: function() {
                this.stage.backgroundColor = '#71c5cf';

                this.load.image('bird', 'assets/bird.png');
                this.load.image('pipe', 'assets/pipe.png');
            },
            
            create: function() {
                // Display the bird on the screen
                this.bird = this.game.add.sprite(100, this.world.centerY-400, 'bird');
                
                // Calculate rhythm
                this.options.rhythm = this.calculateRhythm(this.options.bpm);
                
                // Set the physics system
                this.options.physicsVar = this.gamePhysicsInit(this.bird, this.options.rhythm);
                
                this.rhythmCircle = this.rhythmCircles();
                
            },
            
            update: function() {
                this.rhythmCircle.drawRhythmCircles(500);
                
                if (this.bird.inWorld == false)
                    this.restartGame();
            },
            
            restartGame: function() {
                this.state.start('keyBird');
            },
            
            options: {
                bpm: 120,
                rhythm: 0,
                physicsVar: {}
            },
            
            calculateRhythm: function(bpm) {
                return 60 / bpm * 1000
            },
            
            gamePhysicsInit: function(obj, rhythm) {
                var jumpGravity, jumpVelocity;
                
                this.physics.startSystem(Phaser.Physics.ARCADE);
                
                this.physics.arcade.enable(obj);
                
                jumpGravity = Math.floor((obj.body.height*8)/(rhythm/1000));
                jumpVelocity = -(200-(jumpGravity*(rhythm/1000)*(rhythm/1000))/2)/(rhythm/1000);
                
                obj.body.acceleration.y = jumpGravity;
                
                return {
                    jumpGravity: jumpGravity,
                    jumpVelocity: jumpVelocity
                }
                
            },
            
            birdJump: function() {
                this.bird.body.velocity.y = this.options.physicsVar.jumpVelocity;
            },
            
            rhythmCircles: function() {
                var options, circle, gameState;
                
                gameState = this;
        
                options = {
                    "innerFillColor": "0xFFFF0B",
                    "innerRadius": 50,
                    "outerRadius": 200,
                    "radiusShift": 0
                };
        
                options.radiusShift = function(elapsed) {
                    return options.outerRadius - elapsed*(options.outerRadius-options.innerRadius)/mainState.rhythm
        
                }
        
                circle = this.add.graphics(0, 0);
                
                function drawRhythmCircles(elapsed) {
                    this.ctx.clear();
                    this.ctx.lineStyle(1, "000", "1");
                    this.ctx.drawCircle(gameState.world.centerX, gameState.world.centerY, this.options.radiusShift(elapsed));
                    this.ctx.beginFill(this.options.innerFillColor, 0.5);
                    this.ctx.drawCircle(gameState.world.centerX, gameState.world.centerY, this.options.innerRadius);
                }
        
                return {
                    ctx: circle,
                    options: options,
                    drawRhythmCircles: drawRhythmCircles
                }
        
            },
            
            keyPress: function(event) {
                var charTyped;
                
                charTyped = KB.keyboardTrainer.charRecognition(event);
                
                KB.gameEngine.keyBirdState.birdJump();
            },
            
        },
            
        init: function() {
            
            var game, windowHeight, windowWidth;
            
            windowWidth = $(window).width();
            windowHeight = $(window).height();
            
            game = new Phaser.Game(windowWidth, windowHeight, Phaser.AUTO, 'gameDiv');
            game.state.add('keyBird', this.keyBirdState);
            game.state.start('keyBird');

            document.onkeypress = this.keyBirdState.keyPress;

        }
    }
    
    global.KB = KB;
    
})(this);