// Initialize Phaser, and create a 400x490px this

var windowWidth = $(window).width();
var windowHeight = $(window).height();

// Create our 'main' state that will contain the this
var mainState = {

    preload: function() {
        // Change the background color of the this
        this.stage.backgroundColor = '#71c5cf';

        // Load the bird sprite
        this.load.image('bird', 'assets/bird.png');
        this.load.image('pipe', 'assets/pipe.png');
    },

    options: function() {
        var pipeRowNumber = Math.floor(windowHeight/60);

        return {
            pipeRowNumber: pipeRowNumber
        }
    },

    create: function() {
        this.keytrainer();

        // Set the physics system
        this.physics.startSystem(Phaser.Physics.ARCADE);

        // Display the bird on the screen
        this.bird = this.game.add.sprite(100, this.world.centerY-400, 'bird');

        // Add gravity to the bird to make it fall
        this.physics.arcade.enable(this.bird);

        this.jumpGravity = Math.floor((this.bird.body.height*8)/(this.rhythm/1000));
        this.jumpVelocity = -(200-(this.jumpGravity*(this.rhythm/1000)*(this.rhythm/1000))/2)/(this.rhythm/1000);


        this.bird.body.acceleration.y = this.jumpGravity;

        this.pipes = this.add.group(); // Create a group
        this.pipes.enableBody = true;  // Add physics to the group
        this.pipes.createMultiple(9000, 'pipe'); // Create 20 pipes
        //this.timer = this.time.events.loop(2500, this.addRowOfPipes, this);

        this.score = 0;
        this.labelScore = this.add.text(20, 20, "0", { font: "30px Arial", fill: "#ffffff" });
    },

    keytrainer: function() {
        this.letters = "abcdefghijklmnopqrstuvwxyz";

        var testString = function() {

            var string = '', l = mainState.letters.length, sl = 0;

            sl = 200;

            for (var i = 0; i<sl; i++) {
                string += mainState.letters[Math.floor(Math.random()*l)];
            }

            mainState.letters = string;

        }

        testString();

        var i = 0;
        this.start = new Date().getTime();
        this.bpm = 120;
        this.rhythm = 60 / this.bpm * 1000;

        var elapsed = 0;

        this.k = 0;

        this.testText = this.add.text(50, 10, this.letters, { font: "20px Arial", fill: "#ffffff" });

        this.testText.addColor("#000",0);
        this.testText.addColor("#fff",1);

        this.rhythmText = this.add.text(50, 30, "rhythm", { font: "20px Arial", fill: "#fh3239" });
        this.rhythmText.alpha = 0;

        this.elapsedText = this.add.text(150, 30, elapsed, { font: "20px Arial", fill: "#fh3239" });

        this.rhythmesText = this.add.text(this.world.centerX-7, this.world.centerY-15, this.letters[0], { font: "30px Arial", fill: "#000" });

        this.circle = this.rhythmCircles();

        document.onkeypress = function(evt) {
            evt = evt || window.event;
            var charCode = evt.which || evt.keyCode;
            var charTyped = String.fromCharCode(charCode);

            if (charTyped === mainState.letters[i]) {

                elapsed = mainState.game.time.elapsedSince(mainState.start);
                mainState.start = new Date().getTime();

                mainState.elapsedText.text = elapsed;
                mainState.rhythmesText.text = mainState.letters[i+1];

                if (elapsed < mainState.rhythm + 100 && elapsed > mainState.rhythm - 100) {
                    mainState.rhythmText.alpha = 1;
                } else {
                    mainState.rhythmText.alpha = 0;
                }

                mainState.jump();

                i++;

                mainState.testText.addColor("#fff",i-1);
                mainState.testText.addColor("#000",i);
                mainState.testText.addColor("#fff",i+1);

            }

        };
    },

    update: function() {
        // If the bird is out of the world (too high or too low), call the 'restartGame' function
        if (this.bird.inWorld == false)
            this.restartGame();
        this.physics.arcade.overlap(this.bird, this.pipes, this.restartGame, null, this);

        this.drawRhythmCircles(this.game.time.elapsedSince(mainState.start));
    },

    render: function(){
        game.debug.text('bird speed ' + this.bird.body.speed, 32,732, "#000");

    },

    rhythmCircles: function() {
        var options;

        options = {
            "innerFillColor": "0xFFFF0B",
            "innerRadius": 50,
            "outerRadius": 200,
            "radiusShift": 0
        };

        options.radiusShift = function(elapsed) {

            return options.outerRadius - elapsed*(options.outerRadius-options.innerRadius)/mainState.rhythm

        }

        circle = this.add.graphics(0, 0);

        return {
            ctx: circle,
            options: options
        }

    },

    drawRhythmCircles: function(elapsed) {

        this.circle.ctx.clear();
        this.circle.ctx.lineStyle(1, "000", "1");
        this.circle.ctx.drawCircle(this.world.centerX, this.world.centerY,this.circle.options.radiusShift(elapsed));
        this.circle.ctx.beginFill(this.circle.options.innerFillColor, 0.5);
        this.circle.ctx.drawCircle(this.world.centerX, this.world.centerY, this.circle.options.innerRadius);

    },

    // Make the bird jump
    jump: function() {
        // Add a vertical velocity to the bird
        this.bird.body.velocity.y = this.jumpVelocity;
    },

    // Restart the this
    restartGame: function() {
        // Start the 'main' state, which restarts the this
        this.state.start('main');
    },

    addOnePipe: function(x, y) {
        // Get the first dead pipe of our group
        var pipe = this.pipes.getFirstDead();

        // Set the new position of the pipe
        pipe.reset(x, y);


        // Add velocity to the pipe to make it move left
        pipe.body.velocity.x = -200;

        // Kill the pipe when it's no longer visible
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },

    addRowOfPipes: function() {
        // Pick where the hole will be
        //var hole = Math.floor(Math.random() * this.options().pipeRowNumber) + 1;
        var hole = this.options().pipeRowNumber/2;

        // Add the 6 pipes
        for (var i = 0; i < this.options().pipeRowNumber; i++)
            if (i != hole && i != hole + 1 && i != hole - 1)
                this.addOnePipe(windowWidth, i * 60 + 10);

    }

};

