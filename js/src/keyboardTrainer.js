(function(global) {
    "use strict";
    
    var KB = global.KB || {};
    
    KB.keyboardTrainer = {
    
        loadDictionary: function() {
          var letters;
          
          $.ajax({
              url: "vocabulary/voc1.json",
              dataType: 'json',
              async: false,
              success: function(data) {
                letters = data.letters;
              }
            });
          
          return letters;
          
        },
        
        shuffleString: function(letters, stringLength) {
            var trainString = "",
                letters = letters || "", 
                lettersLength = letters.length,
                stringLength = stringLength || 28,
                i;
                  
            for (i = 0; i<stringLength; i++) {
                trainString += letters[Math.floor(Math.random()*lettersLength)];
            }
            
            return trainString;
            
        },
        
        charRecognition: function(event) {
            var charCode, charTyped;
            
            event = event || window.event;
            charCode = event.which || event.keyCode;
            charTyped = String.fromCharCode(charCode);
            
            return charTyped;
        },
        
        charEqual: function(charTyped, letter) {
            if (charTyped === letter) {
                return true
            }
            
            return false
                
        },
        
        init: function() {
            this.letters = this.loadDictionary();
        }
    }
    
    global.KB = KB;
    
})(this);