(function(KB) {
    
describe("Keyboard trainer", function() {
    
    KB.keyboardTrainer.init();
    
    it("KeyBird namespace is defined", function() {
      expect(KB).toBeDefined();  
    }); 
    
    describe("Basic keyboard trainer functions", function() {
      
        it("keyboard trainer is defined", function() {
          expect(KB.keyboardTrainer).toBeDefined();  
        });
        
        it("English language letters is loaded", function() {
          expect(KB.keyboardTrainer.letters).toEqual("abcdefghijklmnopqrstuvwxyz");  
        });
      
    });
    
    describe("Training string functions", function() {
        
        var trainString;
        
        beforeEach(function(){
            trainString = KB.keyboardTrainer.shuffleString(KB.keyboardTrainer.letters);
        });
        
        afterEach(function(){
            trainString = null;
        });
        
        it("Training string is a string", function(){
            
            expect(trainString).toEqual(jasmine.any(String));
            
        });
        
        it("Shuffled string is needed length", function(){
           
           expect(trainString.length).toEqual(28);
           
           trainString = KB.keyboardTrainer.shuffleString(KB.keyboardTrainer.letters, 200);
           
           expect(trainString.length).toEqual(200);
            
        });
        
        it("Shuffled string contains only loaded letters", function(){
           
           var reg = new RegExp("^["+KB.keyboardTrainer.letters+"]+$","i");
           
           expect(trainString).toMatch(reg);
            
        });
        
    });
    
    describe("Keyboard functions", function() {
        
        function simulateKeyPress(keycode) { 
            var e = jQuery.Event("keypress");
            e.keyCode = keycode;  
            $(document).trigger(e);
            return e
        }
       
        beforeEach(function(){
           event = simulateKeyPress(77);
        });
        
        describe("Char recognition", function(){
           
           it("Char recognized right", function(){
               expect(KB.keyboardTrainer.charRecognition(event)).toEqual("M");
               
               event = simulateKeyPress(122);
               expect(KB.keyboardTrainer.charRecognition(event)).toEqual("z");
               
               event = simulateKeyPress(55);
               expect(KB.keyboardTrainer.charRecognition(event)).toEqual("7");
               
               event = simulateKeyPress(32);
               expect(KB.keyboardTrainer.charRecognition(event)).toEqual(" ");
           });
           
       });
       
       describe("Char equality", function(){
          
          it("Char equals right and returns true or false", function() {
              var letter, charTyped;
              
              charTyped = KB.keyboardTrainer.charRecognition(event);
              
              letter = "M";
              expect(KB.keyboardTrainer.charEqual(charTyped,letter)).toBeTruthy();
              
              letter = "m";
              expect(KB.keyboardTrainer.charEqual(charTyped,letter)).toBeFalsy();
          });
           
       });
        
        
    });
    
});

})(this.KB);